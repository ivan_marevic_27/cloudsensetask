var totalPrice = 0;
var numberOfDifferentMovies = 0;
var finalPrice = 0;
var discountValue= 0;
var names = [];

function addRow() {

  //Dovhacanje i provjera ispravnosti podataka,  ne smije biti jedno od dva polja prazno te cijena ne smije biti negativna
  var movieName = document.getElementById("movieName").value;
  var moviePrice = document.getElementById("moviePrice").value;

  if (movieName == "" || moviePrice=="" || parseFloat(moviePrice) < 0) {
    return;
  }

  //Dodavanje novog redka u tablicu
  var table = document.getElementById("myTable").getElementsByTagName("tbody")[0];

  var rowsLength = table.rows.length;

  var newRow = table.insertRow();

  var cel1 = newRow.insertCell(0);
  var cel2 = newRow.insertCell(1);
  var cel3 = newRow.insertCell(2);

  cel1.innerHTML = rowsLength + 1;
  cel2.innerHTML = movieName;
  cel3.innerHTML = moviePrice;

  names.push(table.rows[rowsLength].cells[1].innerHTML);

  //Postavljanje vrijednosti parametara ukupno,popust i za platiti
  var uniqueSet = new Set(names);
  var array = [...uniqueSet];

  totalPrice = totalPrice + parseFloat(table.rows[rowsLength].cells[2].innerHTML);


  if(array.length>2){
      discountValue = totalPrice * 0.05;
  }

  discountValue = parseFloat(discountValue.toFixed(2));
  finalPrice = totalPrice - discountValue;

  
  //Resetiranje vrijednosti unutar forme
  document.getElementById("movieName").value = "";
  document.getElementById("moviePrice").value = "";

  //Postavljanje novog prikaza cijena
  document.getElementById("totalPrice").innerHTML = "Ukupno: " + totalPrice.toFixed(2) + " HRK";

  if(discountValue == 0){
    document.getElementById("discount").innerHTML = "Popust: " + discountValue.toFixed(2) + " HRK";
  } else {
    document.getElementById("discount").innerHTML = "Popust: -" + discountValue.toFixed(2) + " HRK";

  }

  document.getElementById("endPrice").innerHTML = "Za platiti: " + finalPrice.toFixed(2) + " HRK";

}
